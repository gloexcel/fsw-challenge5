const express = require('express');
const router = express.Router();
const exampleController = require('./controllers/example-controller');

router.get('/', exampleController.home);

router.get('/game', exampleController.game);

router.get('/info', exampleController.info);

router.get('/bio', exampleController.bio);

router.get('/product/:id', exampleController.productId);

router.get('/product/', exampleController.product);

router.post('/login/', exampleController.login);

module.exports = router